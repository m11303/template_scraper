import os
import traceback

from bs4 import BeautifulSoup
import httpx


def run(parameters, agent, output_path, logger) -> bool:
    logger.info(f"starting scraper with following parameters: {parameters}")
    data = []
    with httpx.Client() as client:
        for url in parameters.get("targets"):
            try:
                logger.info(url)
                if "http" not in url:
                    url = f"https://www.{url}"
                res = client.get(url)
                soup = BeautifulSoup(res.text, 'html.parser')
                title = soup.find('title').text
                data.append(title)
                if parameters.get("img"):
                    img_tags = soup.find_all('img')
                    images = [str(img.get('src')) for img in img_tags]
                    data.extend(images)
                if parameters.get("links"):
                    a_tags = soup.find_all('a')
                    links = [str(a.get('href')) for a in a_tags]
                    data.extend(links)
            except:
                logger.error("error in script. see traceback")
    if data:
        if parameters.get('name'):
            file_name = parameters.get('name')
        else:
            file_name = "result"
        logger.info(f"saving results at {output_path}: {data}")
        with open(os.path.join(output_path, f"{file_name}.txt"), encoding="utf-8", mode="w") as file:
            for line in data:
                file.write(line + "\n")
        return True
    else:
        logger.info(f"no results found")
        return False




if __name__ == '__main__':
    import logging as logger

    logger.basicConfig(level=logger.DEBUG)
    run({'targets': ["dba.dk"], "links": True, "img": True}, "", "", logger)
